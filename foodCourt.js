function item1Click() {
  let item = prompt("How many Burger would you like?");
  saveItemVal(item, 1);
}

function item2Click() {
  let item = prompt("How many Coffee would you like?");
  saveItemVal(item, 2);
}

function item3Click() {
  let item = prompt("How many Muffins would you like?");
  saveItemVal(item, 3);
}

function item4Click() {
  let item = prompt("How many Cookies would you like?");
  saveItemVal(item, 4);
}

function saveItemVal(item, itemNum) {
  let val = parseInt(`${item}`);
  if (!isNaN(val)) {
    document.getElementById("item" + itemNum + "-qnt-mark").innerHTML = "X ";
    document.getElementById("item" + itemNum + "-qnt").innerHTML = val;
  } else if (item == null) {
    document.getElementById("item" + itemNum + "-qnt-mark").innerHTML = "X ";
    document.getElementById("item" + itemNum + "-qnt").innerHTML = 0;
  } else {
    item = prompt("Please enter digits only");
    val = parseInt(`${item}`);
    saveItemVal(val, itemNum);
  }
}

function checkoutClick() {
  let nameInput = prompt("Please enter your name: ");
  if (isNotEmpty(nameInput)) {
    let element = document.getElementById("receipt-div");
    element.classList.remove("hide");
    let labelStr = "Dear <span class='name-lbl'>" + nameInput + ",</span>";
    labelStr += "</br> ";
    document.getElementById("name").innerHTML = labelStr;
    makeTableHTML();
  } else {
    checkoutClick();
  }
}

function isNotEmpty(str) {
  return str != null && str.trim().length > 0;
}

function makeTableHTML() {
  var result = "<span> Please find your final receipt :</span>";
  result += "<table class='tbl margin-top' border ='1'>";
  result += "<tr>";
  result += " <th> Item </th> ";
  result += " <th> Quantity </th> ";
  result += " <th> Total </th> ";
  result += " </tr> ";
  let selectedItem = false;
  let allItemTotal = 0;
  for (let i = 1; i <= 4; i++) {
    let itemQnt = document.getElementById("item" + i + "-qnt").innerHTML;
    if (itemQnt > 0) {
      let itemRate = document.getElementById("item" + i + "-rate").innerHTML;
      selectedItem = true;
      let itemTotal = parseFloat(itemQnt * itemRate).toFixed(2);

      let itemName = document.getElementById("item-name" + i).innerText;
      result += "<tr>";
      result += "<td> " + itemName + " </td>";
      result += "<td class='algn-right'> " + itemQnt + " </td>";
      result +=
        "<td class='algn-right'> $" + formatNumber(itemTotal) + " </td>";
      result += " </tr> ";
      allItemTotal += parseFloat(itemTotal);
    }
  }

  let body = document.getElementById("receipt-inner-div");
  if (selectedItem) {
    let taxAmount = parseFloat((allItemTotal * 13) / 100).toFixed(2);
    result += "<tr>";
    result += "<td colspan = '2' class='algn-right'> HST @ 13% </td>";
    result += "<td class='algn-right'> $" + formatNumber(taxAmount) + "</td>";
    result += "</tr>";

    let finalAmount = (
      parseFloat(allItemTotal) + parseFloat(taxAmount)
    ).toFixed(2);

    result += "<tr>";
    result += "<td colspan = '2' class='algn-right'> Total </td>";
    result +=
      "<td class='algn-right'><strong> $" +
      formatNumber(finalAmount) +
      " </strong></td>";
    result += "</tr>";
    result += "</table>";

    document.getElementById("receipt-inner-div").innerHTML = result;
  } else {
    document.getElementById("receipt-inner-div").innerHTML =
      "<span class='err-msg'> Please select item for final receipt </span>";
  }
}

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
