const express = require("express");
const path = require("path");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
dotenv.config({ path: "./config.env" });
const viewRouter = require("./routes/viewRoutes");
const userRouter = require("./routes/userRoutes");
const session = require("express-session");
const app = express();
app.use(express.urlencoded({ extended: true }));
// app.use(express.json());
app.use(
  session({
    secret: "superrandomsecret",
    resave: false,
    saveUninitialized: false,
  })
);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "public")));
app.use("/", viewRouter);
app.use("/user", userRouter);

app.listen(process.env.PORT, () => {
  console.log(`Server is up and running on port ${process.env.PORT}`);
});

const db = `mongodb+srv://symoti:admin123@cluster0.8v7jy.mongodb.net/pixie?retryWrites=true&w=majority`;

mongoose.connect(db, (err) => {
  if (err) {
    console.log("Error connecting to databse");
  } else {
    console.log("Database connected");
  }
});
