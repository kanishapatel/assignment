const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    firstName: String,
    lastName: String,
    mobile: Number,
    province: String,
    city: String,
    email: String,
    address: String,
    paid: {
      type: Boolean,
      default: false,
    },
    totalAmount: Number,
    totalTax: Number,
    totalPayble: Number,
    cart: [
      {
        id: String,
        photo: String,
        title: String,
        price: String,
        quantity: Number,
      },
    ],
  },
  { timestamps: true }
);

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;
