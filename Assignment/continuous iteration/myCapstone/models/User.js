const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    userId: String,
    firstName: String,
    lastName: String,
    email: String,
    contactNumber: Number,
    address: String,
    userName: String,
    password: Object,
    createdBy: String,
    updatedBy: String,
    createdAt: Date,
    updatedAt: Date,
    roleName: String,
  },
  { timestamps: true }
);

const User = mongoose.model("User", userSchema);
module.exports = User;
