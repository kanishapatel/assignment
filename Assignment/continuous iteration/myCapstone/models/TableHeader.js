const mongoose = require("mongoose");

const tableHeaderSchema = new mongoose.Schema(
  {
    id: String,
    headerName: String,
    headerValue: String,
    createdBy: String,
    updatedBy: String,
    createdAt: Date,
    UpdatedAt: Date,
  },
  { timestamps: true }
);

const TableHeader = mongoose.model("TableHeader", tableHeaderSchema);
module.exports = TableHeader;
