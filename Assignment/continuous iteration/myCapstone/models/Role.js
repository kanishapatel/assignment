const mongoose = require("mongoose");

const roleSchema = new mongoose.Schema(
  {
    roleId: String,
    roleName: String,
  },
  { timestamps: true }
);

const Role = mongoose.model("Role", roleSchema);
module.exports = Role;
