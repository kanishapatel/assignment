const express = require("express");
const userController = require("./../controller/userController");

const router = express.Router();
const { check, validationResult, Result } = require("express-validator");

//user
router
  .route("/add-user")
  .get(userController.addUserForm)
  .post(
    [
      check("firstName", "Must have a first name.").notEmpty(),
      check("lastName", "Must have a last name.").notEmpty(),
      check("contactNumber", "Must have a contact number.").notEmpty(),
      check("email", "Must have an email address.").notEmpty(),
      check("address", "Must have a address.").notEmpty(),
      check("userName", "Must have a user name.").notEmpty(),
    ],
    userController.addUser
  );

router.route("/list").get(userController.userList);
router.route("/get/:id").get(userController.getUser);
router
  .route("/edit/:id")
  .get(userController.editUser)
  .post(
    [
      check("firstName", "Must have a first name.").notEmpty(),
      check("lastName", "Must have a last name.").notEmpty(),
      check("contactNumber", "Must have a contact number.").notEmpty(),
      check("email", "Must have an email address.").notEmpty(),
      check("address", "Must have a address.").notEmpty(),
      check("userName", "Must have a user name.").notEmpty(),
    ],
    userController.editUserData
  );

router.route("/delete/:id").get(userController.deleteUserForm);
//.delete(userController.delete);

module.exports = router;
