const express = require("express");
const userController = require("./../controller/userController");
const loginController = require("./../controller/loginController");
const dashboardController = require("./../controller/dashboardController");
const configController = require("./../controller/configController");
const tableController = require("./../controller/tabelController");
const initController = require("./../controller/initController");
const router = express.Router();
const { check, validationResult, Result } = require("express-validator"); // ES6 standard for destructuring an object

router.route("/").get(userController.index);

//login
router
  .route("/login")
  .get(loginController.loginRedirect)
  .post(loginController.login);

//init data to run first time
router.route("/role/add").get(initController.addRoles);

router.route("/logout").get(loginController.logout);

//dashboard
router.route("/dashboard").get(dashboardController.get);
router.route("/conf").get(configController.configRedirect);
router.route("/conf/header").post(configController.updateTableHeader);
router.route("/conf/header").get(configController.getHeader);
router.route("/conf/header/add").get(configController.addHeader);
router.route("/conf/role").get(configController.getRoles);
router.route("/conf/color").get(configController.colorRedirect);

router.route("/table").get(tableController.get);
router.route("/table").post(tableController.post);
module.exports = router;
