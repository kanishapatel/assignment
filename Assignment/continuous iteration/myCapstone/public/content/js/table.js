$(document).ready(() => {
  const tableData = $("#serverdata").data("test");
  console.log(tableData);
  const backgroundColor = {
    1: "white",
    2: "green",
    3: "orange",
  };

  $("#languages").html(
    tableData.header
      .map((v, i) => `<option value="${i}"> ${v.value}</option>`)
      .join("")
  );

  $("#languages").val(tableData.header.map((v, i) => i));

  generateTable(tableData, backgroundColor, true);

  $(document).on("mousedown", function (e) {
    if (!$(e.target).parents(".custom-menu").length > 0) {
      $(".custom-menu").hide(100);
    }
  });

  // If the menu element is clicked
  $(".custom-menu li").click(function () {
    const cellTextId = $("#txt_id").val();
    const cell = $("#" + cellTextId).parent();
    tableData.column[cell.attr("id").split("_")[1]][cell.attr("columnid")][
      "colorId"
    ] = $(this).data("action").toString();
    cell.attr(
      "style",
      "background-color:" + backgroundColor[$(this).data("action")]
    );
    $(".custom-menu").hide(100);
  });

  $("#languages").multiSelect();

  $("#languages").on("change", function (val) {
    console.log($(this).val());
    const tableConf = $(this).val();
    const filteredTableData = {
      header: [],
      column: [],
    };

    filteredTableData.header = tableData.header.filter(
      (v, i) => tableConf.indexOf(i.toString()) != -1
    );
    filteredTableData.column = tableData.column.map((v) =>
      v.filter((x, i) => tableConf.indexOf(i.toString()) != -1)
    );
    console.log(filteredTableData);
    generateTable(filteredTableData, backgroundColor, true);
  });

  $("#submitformbtn").on("click", () => {
    $("#tableData").val(JSON.stringify(tableData));
    console.log($("#tableData").val());
    $("#myform").trigger("submit");
  });
});

const generateTable = (data, backgroundColor, isEdit) => {
  const tableHeader = `<tr>${data.header
    .map((v) => `<th>${v.value}</th>`)
    .join("")}</tr>`;
  const tableRows = data.column.reduce((cv, v, vi) => {
    return (cv += `<tr id="row_${vi}">${v
      .map((x, xi) => {
        return `<td style="background-color:${
          backgroundColor[x.colorId]
        }" id="cell_${vi}_${xi}" columnid="${data.header[xi].columId}">${
          isEdit
            ? '<input type="text" id="cell_text_' +
              vi +
              "_" +
              xi +
              '" class="cell-input" value="' +
              x.fieldValue +
              '" />'
            : x.fieldValue
        }</td>`;
      })
      .join("")}</tr>`);
  }, "");
  $("#main").html(
    data.header.length > 0
      ? `<table class="data-table">
        <thead>
            ${tableHeader}
        </thead>
        <tbody>
          ${tableRows}
        </tbody>
      </table>`
      : "Please select columns in order to show the table"
  );

  $("table input").on("input", function (e) {
    e.preventDefault();
    data.column[this.parentNode.id.split("_")[1]][
      this.parentNode.getAttribute("columnid")
    ]["fieldValue"] = e.target.value;
  });
  $("table input").on("contextmenu", function (event) {
    // Avoid the real one
    event.preventDefault();
    $("#txt_id").val(this.id);
    // Show contextmenu
    $(".custom-menu")
      .finish()
      .toggle(100)
      // In the right position (the mouse)
      .css({
        top: event.pageY + "px",
        left: event.pageX + "px",
      });
  });
};
