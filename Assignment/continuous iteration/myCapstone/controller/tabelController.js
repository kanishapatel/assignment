const fs = require("fs");
const path = require("path");

exports.get = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  const tableData = fs.readFileSync(path.join("./models/table.json"));
  res.render("table/table", { data: JSON.stringify(JSON.parse(tableData)) });
};

exports.post = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  console.log(req.body);
  res.render("/");
};
