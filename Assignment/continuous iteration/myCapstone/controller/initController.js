const User = require("../models/User.js");
const Role = require("../models/Role.js");
const roleJson = require("./../data/role.json");

exports.addRoles = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  Role.find({}).exec(function (err, roleList) {
    if (
      roleList.length == 0 &&
      roleJson != null &&
      roleJson.data != null &&
      roleJson.data.length > 0
    ) {
      roleJson.data.forEach((element) => {
        var role = new Role(element);
        role.createdBy = "System";
        role.createdAt = Date.now();
        role.save().then(function () {});
      });
      res.render("message", {
        message: "Roles have been added successfully !!",
      });
    } else {
      res.render("message", {
        message: "Roles are already exists !!",
      });
    }
  });
};
