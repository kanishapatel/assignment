exports.get = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  res.render("dashboard/dashboard");
};

exports.configRedirect = (req, res) => {
  res.render("conf/config");
};
