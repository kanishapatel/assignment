const User = require("./../models/User");
const { check, validationResult, Result } = require("express-validator");
const { encrypt, decrypt } = require("./../lib/crypto");
var moment = require("moment");

exports.index = (req, res) => {
  res.render("index");
};
exports.getUser = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  var id = req.params.id;
  if (id) {
    User.findOne({ _id: id }).exec(function (err, user) {
      if (user) {
        var role = req.session.role;
        res.render("user/user", { user, moment, role });
      } else {
        res.render("message", {
          message: "Sorry, Something Went Wrong1",
        });
      }
    });
  }
};
exports.userList = (req, res) => {
  if (!req.session.userLoggedIn) {
    return res.redirect("/login");
  }
  User.find({}).exec(function (err, users) {
    var role = req.session.role;
    res.render("user/userList", { users, moment, role });
  });
};

exports.addUserForm = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  res.render("user/userForm");
};
function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (checkRegex(email, re)) {
    return true;
  }
  return false;
}
function validatePhoneNumber(phoneNumber) {
  const regex = /^\d{10}$/;
  if (checkRegex(phoneNumber, regex)) {
    return true;
  }
  return false;
}
function checkRegex(userInput, regex) {
  if (regex.test(userInput)) {
    return true;
  } else {
    return false;
  }
}

exports.addUser = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  const errors = validationResult(req).array();
  if (!validatePhoneNumber(req.body.contactNumber)) {
    errors.push({
      msg: "Contact number should be in xxxxxxxxxx format.",
    });
  }
  if (!validateEmail(req.body.email)) {
    errors.push({
      msg: "Email address should be in proper format.",
    });
  }
  if (errors.length > 0) {
    res.render("user/userForm", { errors });
  } else {
    //admin adds password for new user
    const hash = encrypt("SymotiUser");
    var pageData = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      contactNumber: req.body.contactNumber,
      userName: req.body.userName,
      address: req.body.address,
      email: req.body.email,
      password: hash,
      roleName: "user",
    };
    var user = new User(pageData);
    // save the user
    user.save().then(function () {});
    res.redirect("/user/list");
  }
};
exports.editUser = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  var id = req.params.id;
  User.findOne({ _id: id }).exec(function (err, user) {
    if (user) {
      res.render("user/userEditForm", {
        user,
      });
    } else {
      res.render("message", {
        message: "Sorry, Something Went Wrong2",
      });
    }
  });
};

exports.editUserData = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  var id = req.params.id;
  User.findOne({ _id: id }).exec(function (err, user) {
    if (user) {
      const errors = validationResult(req).array();
      console.log(errors);
      if (errors.length > 0) {
        res.render("user/userEditForm", { user, errors });
      } else {
        var id = req.params.id;
        User.findOne({ _id: id }, function (err, user) {
          user.firstName = req.body.firstName;
          user.lastName = req.body.lastName;
          user.contactNumber = req.body.contactNumber;
          user.userName = req.body.userName;
          user.address = req.body.address;
          user.email = req.body.email;
          user.save();
          res.redirect("/user/list");
        });
      }
    }
  });
  //}
};

exports.deleteUserForm = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  var id = req.params.id;
  User.findByIdAndDelete({ _id: id }).exec(function (err) {
    res.redirect("/user/list");
  });

  //}
};
exports.delete = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  var id = req.params.id;
  User.findByIdAndDelete({ _id: id }).exec(function (err) {
    res.redirect("/user/list");
  });
};
