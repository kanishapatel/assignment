const TableHeader = require("./../models/TableHeader");
const Role = require("./../models/Role");
const headers = require("./../data/header.json");

exports.configRedirect = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  res.render("conf/config");
};
exports.updateTableHeader = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  TableHeader.find({}).exec(function (err, headers) {
    var dbHeaders = headers;
    if (req.body != null && dbHeaders != null) {
      for (let key in req.body) {
        let obj = dbHeaders.find((o) => o.headerName === key);
        if (obj != null && obj.headerValue != req.body[key]) {
          obj.headerValue = req.body[key];
          obj.save();
        }
      }
      res.render("message", {
        message: "Table headers have been updated successfully !!",
      });
    }
  });
};

exports.addHeader = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  TableHeader.find({}).exec(function (err, dbHeaderList) {
    if (
      dbHeaderList.length == 0 &&
      headers != null &&
      headers.data != null &&
      headers.data.length > 0
    ) {
      headers.data.forEach((element) => {
        var header = new TableHeader(element);
        //add session details for createdby and updatedby
        header.save().then(function () {});
      });
      res.render("message", {
        message: "Table headers have been added successfully !!",
      });
    } else {
      res.render("message", {
        message: "Table are already exists !!",
      });
    }
  });
};

exports.getHeader = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  TableHeader.find({}).exec(function (err, data) {
    var headers = data.sort((a, b) => a.id - b.id);
    res.render("conf/tableHeader", { headers });
  });
};
exports.getRoles = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  Role.find({}).exec(function (err, roles) {
    res.render("conf/role", { roles });
  });
};
exports.colorRedirect = (req, res) => {
  if (!req.session.userLoggedIn || req.session.role != "admin") {
    return res.redirect("/login");
  }
  res.render("conf/color");
};
