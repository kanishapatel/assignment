const User = require("./../models/User.js");
const { encrypt, decrypt } = require("./../lib/crypto");
const crypto = require("crypto");

exports.loginRedirect = (req, res) => {
  // const hash = encrypt("admin");
  // const text = decrypt(hash);
  //ONLY ONE TIME RUN THIS
  // var pageData = {
  //   firstName: "admin",
  //   lastName: "admin",
  //   userName: "admin",
  //   password: hash,
  //   createdBy: "System",
  //roleName: "admin",
  //   createdAt: Date.now(),
  //   roleId: 1,
  // };
  // var user = new User(pageData);
  // user.save().then(function () {});
  res.render("login");

  // if (!req.session.userLoggedIn) {
  //   res.render("login");
  // } else {
  //   res.redirect("/dashboard");
  // }
};

exports.login = (req, res) => {
  var user = req.body.username;
  var pass = req.body.password;

  User.findOne({ userName: user }).exec(function (err, admin) {
    if (admin) {
      var dbPassword = admin.password;
      const decryptedData = decrypt(dbPassword);
      if (decryptedData == pass) {
        req.session.userName = admin.userName;
        req.session.userLoggedIn = true;
        req.session.role = admin.userName;
        res.redirect("/dashboard");
      } else {
        res.render("login", {
          error: "Sorry, can't login! Please enter valid credentials.",
        });
      }
    } else {
      res.render("login", {
        error: "Sorry, can't login! Please enter valid credentials.",
      });
    }
  });
};
exports.logout = (req, res) => {
  req.session.username = "";
  req.session.userLoggedIn = false;
  req.session.role = "";
  req.session.destroy();
  return res.redirect("/login");
};
