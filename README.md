# Project Title

Food court

---
## Requirements

Requirement is to make project for food court. Where user can order their food and after that they need to do confirmation. User will get their receipt as well.

### Node

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).


If the installation was successful, you should be able to run the following command.


    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.


    $ npm install npm -g


## Install


    $ git clone https://kanishaPatel@bitbucket.org/kanishapatel/assignment.git
  

## Configure app


	No configuration is required

## Running the project


    node foodCourt.js
